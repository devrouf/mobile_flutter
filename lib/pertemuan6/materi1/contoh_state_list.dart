import 'package:faker/faker.dart';
import 'package:flutter/material.dart';
import 'package:mobile_flutter/pertemuan6/materi1/NamaOrang.dart';

class ContohStateList extends StatefulWidget {
  const ContohStateList({Key? key}) : super(key: key);

  @override
  _ContohStateListState createState() => _ContohStateListState();
}

class _ContohStateListState extends State<ContohStateList> {
  List<NamaOrang>? _listNamaOrang;

  @override
  void initState() {
    super.initState();
    _listNamaOrang = <NamaOrang>[];
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('State List Object'),
          actions: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: IconButton(icon: Icon(Icons.add), onPressed: _addData),
            )
          ],
        ),
        body: Builder(
          builder: (BuildContext ctx) {
            if (_listNamaOrang!.isEmpty) {
              return Container(
                alignment: Alignment.center,
                child: ElevatedButton(
                    child: Text('Isi Data Array'), onPressed: _isiData),
              );
            }

            return ListView.builder(
                itemCount: _listNamaOrang!.length,
                itemBuilder: (BuildContext ctx, int index) {
                  return ListTile(
                    title: Text('${_listNamaOrang![index].namaDepan!}'),
                    subtitle: Text('Index ke ${index.toString()}'),
                    onLongPress: () {
                      showDialog(
                          context: context,
                          builder: (BuildContext context) {
                            return AlertDialog(
                              content: Text('Apakah Anda yakin ingin hapus?'),
                              actions: [
                                ElevatedButton(
                                    onPressed: () {
                                      _deleteDataOneByOne(index);
                                      Navigator.of(context).pop();
                                    },
                                    child: Text('Iya')),
                                ElevatedButton(
                                    onPressed: () {
                                      Navigator.of(context).pop();
                                    },
                                    child: Text('Tidak'))
                              ],
                            );
                          });
                    },
                  );
                });
          },
        ));
  }

  void _isiData() {
    for (int i = 0; i <= 10; i++) {
      setState(() {
        _listNamaOrang!.add(NamaOrang(
            namaDepan: '${Faker().person.name()}',
            namaBelakang: '${Faker().person.name()}'));
      });
    }
  }

  void _addData() {
    setState(() {
      _listNamaOrang!.add(NamaOrang(
          namaDepan: '${Faker().person.name()}',
          namaBelakang: '${Faker().person.name()}'));
    });
  }

  void _deleteDataOneByOne(int index) {
    setState(() {
      _listNamaOrang!.removeAt(index);
    });
  }
}
