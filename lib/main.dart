import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:mobile_flutter/pertemuan/pertemuan.dart';
import 'package:mobile_flutter/pertemuan10/pertemuan10.dart';
import 'package:mobile_flutter/pertemuan11/pertemuan11.dart';
import 'package:mobile_flutter/pertemuan2/pertemuan2.dart';
import 'package:mobile_flutter/pertemuan3/pertemuan3.dart';
import 'package:mobile_flutter/pertemuan4/pertemuan4.dart';
import 'package:mobile_flutter/pertemuan5/pertemuan5.dart';
import 'package:mobile_flutter/pertemuan6/materi1/contoh_navigasi.dart';
import 'package:mobile_flutter/pertemuan6/pertemuan6.dart';
import 'package:mobile_flutter/pertemuan7/pertemuan7.dart';
import 'package:mobile_flutter/pertemuan8/pertemuan8.dart';
import 'package:mobile_flutter/pertemuan9/pertemuan9.dart';
import 'package:mobile_flutter/tugasakhir/tugasakhir.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();

  await Firebase.initializeApp();

  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'PT SISI',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'SISI Flutter Academy'),
      debugShowCheckedModeBanner: true,
      // home: ContohNavigasi(),
    );
  }
}

class Materi {
  String title;
  Widget page;

  Materi(this.title, this.page);
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    var menus = [
      Materi('Perkenalan', PertemuanPage()),
      Materi('Pertemuan 2', Pertemuan2Page()),
      Materi('Pertemuan 3', Pertemuan3Page()),
      Materi('Pertemuan 4', Pertemuan4Page()),
      Materi('Pertemuan 5', Pertemuan5Page()),
      Materi('Pertemuan 6', Pertemuan6Page()),
      Materi('Pertemuan 7', Pertemuan7Page()),
      Materi('Pertemuan 8', Pertemuan8Page()),
      Materi('Pertemuan 9', Pertemuan9Page()),
      Materi('Pertemuan 10', Pertemuan10Page()),
      Materi('Pertemuan 11', Pertemuan11Page()),
      Materi('Tugas Akhir', TugasAkhirPage()),
    ];
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: ListView.builder(
        itemBuilder: (bc, idx) {
          var menu = menus[idx];
          return ListTile(
            title: Text(menu.title),
            trailing: Icon(Icons.arrow_forward_ios_sharp),
            onTap: () {
              Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (route) => menu.page,
                ),
              );
            },
          );
        },
        itemCount: menus.length,
      ),
    );
  }
}


