import 'package:flutter/material.dart';
import 'package:mobile_flutter/pertemuan5/materi1/contoh_bottomsheet.dart';
import 'package:mobile_flutter/pertemuan5/materi1/contoh_dialog.dart';
import 'package:mobile_flutter/pertemuan5/materi1/contoh_snackbar.dart';

class P5Metari1 extends StatefulWidget {
  const P5Metari1({Key? key}) : super(key: key);

  @override
  _P5Metari1State createState() => _P5Metari1State();
}

class _P5Metari1State extends State<P5Metari1> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Materi 1"),
      ),
      body: ListView(
        children: [
          list(
            title: "Contoh Dialog",
            onTap: () {
              Navigator.of(context).push(
                MaterialPageRoute(builder: (route) => ContohDialog())
              );
            }
          ),
          list(
            title: "Contoh BottomSheet",
            onTap: () {
              Navigator.of(context).push(
                MaterialPageRoute(builder: (route) => ContohBottomSheet())
              );
            }
          ),
          list(
            title: "Contoh SnackBar & Toast",
            onTap: () {
              Navigator.of(context).push(
               MaterialPageRoute(builder: (route) => ContohSnackBar())
              );
            }
          )
        ],
      ),
    );
  }

  Widget list({String? title, GestureTapCallback? onTap}) {
    return ListTile(
      title: Text('$title'),
      trailing: Icon(Icons.arrow_right),
      onTap: onTap,
    );
  }
}
