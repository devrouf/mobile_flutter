import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
// flutter pub add fluttertoast

class ContohSnackBar extends StatefulWidget {
  const ContohSnackBar({Key? key}) : super(key: key);

  @override
  _ContohSnackBarState createState() => _ContohSnackBarState();
}

class _ContohSnackBarState extends State<ContohSnackBar> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("SnackBar dan Toast"),
      ),
      body: Container(
        alignment: Alignment.center,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: InkWell(
                child: Text('Contoh SnackBar'),
                onTap: () => ScaffoldMessenger.of(context).showSnackBar(showSnackBar(context)),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: InkWell(
                child: Text('Contoh Toast'),
                onTap: () =>  showToast(),
              ),
            )
          ],
        ),
      ),
    );
  }

  SnackBar showSnackBar(BuildContext context) {
    return SnackBar(
        content: Text('Ini contoh SnackBar!'),
        action: SnackBarAction(
          label: 'Undo',
          onPressed: () {

          },
        ),
    );
  }

  void showToast() {
    Fluttertoast.showToast(
        msg: "Ini Contoh Toast",
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.CENTER,
        timeInSecForIosWeb: 1,
        backgroundColor: Colors.red,
        textColor: Colors.white,
        fontSize: 16.0
    );
  }
}
