import 'package:flutter/material.dart';
import 'package:mobile_flutter/pertemuan5/materi3/box_warna.dart';

class ContohNoState extends StatefulWidget {
  const ContohNoState({Key? key}) : super(key: key);

  @override
  _ContohNoStateState createState() => _ContohNoStateState();
}

class _ContohNoStateState extends State<ContohNoState> {
  List<Widget> tiles = [
    // BoxWarna(key: UniqueKey()),
    // BoxWarna(key: UniqueKey()),
    BoxWarna(),
    BoxWarna(),
  ];

  @override
  Widget build(BuildContext context) {
    print("rebuild");
    return Scaffold(
      appBar: AppBar(title: Text("SFA 2021 - State")),
      body: SafeArea(
        child: Column(children: [
          Text("Padahalan menggunakan stateful,"),
          Text("namun isi listview tidak mau berubah,"),
          Text("bagaimana dengan state management?"),
          Expanded(child: Row(children: tiles)),
          Expanded(child: ListView(children: tiles)),
          tiles[0],
        ]),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          setState(() {
            tiles.insert(1, tiles.removeAt(0));
          });
        },
        child: Icon(Icons.swap_horiz),
      ),
    );
  }
}

