import 'package:flutter/material.dart';
import 'package:mobile_flutter/utils/unique_color_generator.dart';

class BoxWarna extends StatefulWidget {
  // const BoxWarna({Key? key}) : super(key: key);
  BoxWarna({Key? key}) : super(key: key);
  final Color _color = UniqueColorGenerator.getColor();

  @override
  _BoxWarnaState createState() => _BoxWarnaState();
}

class _BoxWarnaState extends State<BoxWarna> {
  // Color _color = UniqueColorGenerator.getColor();
  Color? _color;

  @override
  void initState() {
    super.initState();
    _color = UniqueColorGenerator.getColor();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      // color: _color,
      color: widget._color,
      width: 128,
      height: 128,
    );
  }
}
