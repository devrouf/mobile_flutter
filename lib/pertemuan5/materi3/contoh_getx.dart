import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mobile_flutter/pertemuan5/materi3/contoh_sm_controller.dart';

class ContohGetX extends StatelessWidget {
  final controller = Get.put(ContohSMController());

  @override
  Widget build(BuildContext context) {
    print("rebuild");
    return Scaffold(
      appBar: AppBar(title: Text("SFA 2021 - SM GetX")),
      body: Obx(() => Column(
        children: [
          Text("Kini listview juga bisa update tampilan,"),
          Text("bahkan saat menggunakan stateless,"),
          Text("luar biasa, recommended!"),
          Expanded(child: Row(children: controller.tiles,)),
          Expanded(child: ListView(children: controller.tiles,)),
          controller.tiles[0],
        ],
      )),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          controller.updateTiles();
        },
        child: Icon(Icons.swap_horiz),
      ),
    );
  }
}
