import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mobile_flutter/pertemuan5/materi3/box_warna.dart';

class ContohSMController extends GetxController {
  RxList<Widget> tiles = [
    BoxWarna(key: UniqueKey()),
    BoxWarna(key: UniqueKey()),
    BoxWarna(key: UniqueKey()),
    // BoxWarna(),
    // BoxWarna(),
    // BoxWarna(),
    // BoxWarna(),
  ].obs;

  void updateTiles() {
    tiles.add(tiles.removeAt(0));
    // refresh();
  }
}
