import 'package:flutter/material.dart';

class ContohStateful extends StatefulWidget {
  const ContohStateful({Key? key}) : super(key: key);

  @override
  _ContohStatefulState createState() => _ContohStatefulState();
}

class _ContohStatefulState extends State<ContohStateful> {
  int nilai = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("SFA - Contoh Stateful")),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.refresh),
        onPressed: () {
          if (nilai >= 3) {
            showDialog(
              context: context,
              builder: (bc) => AlertDialog(
                title: Text("Alert $nilai"),
              ),
            ).then((value) => nilai = 0);
            // );
            // nilai = 0;
          } else {
            nilai++;
          }
          setState(() {});
        },
      ),
      body: Center(child: Text("nilai $nilai")),
    );
  }
}
