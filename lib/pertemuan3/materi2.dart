import 'package:flutter/material.dart';
import 'package:mobile_flutter/pertemuan3/materi2/contoh_autocomplete.dart';
import 'package:mobile_flutter/pertemuan3/materi2/contoh_dropdown.dart';
import 'package:mobile_flutter/pertemuan3/materi2/contoh_switch.dart';
import 'package:mobile_flutter/pertemuan3/materi2/contoh_switchtile.dart';

class P3Materi2 extends StatefulWidget {
  const P3Materi2({Key? key}) : super(key: key);

  @override
  _P3Materi2State createState() => _P3Materi2State();
}

class _P3Materi2State extends State<P3Materi2> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text("Materi 2")),
        body: ListView(
          children: [
            list(
                title: "Contoh Switch",
                onTap: () {
                  Navigator.of(context).push(
                    MaterialPageRoute(builder: (route) => ContohSwitch()),
                  );
                }),
            list(
                title: "Contoh DropDown",
                onTap: () {
                  Navigator.of(context).push(
                    MaterialPageRoute(builder: (route) => ContohDropdown()),
                  );
                }),
            list(
                title: "Contoh AutoComplete",
                onTap: () {
                  Navigator.of(context).push(
                    MaterialPageRoute(builder: (route) => AutoComplete()),
                  );
                }),
            list(
                title: "Contoh SwitchTile",
                onTap: () {
                  Navigator.of(context).push(
                    MaterialPageRoute(builder: (route) => ContohSwitchTile()),
                  );
                })
          ],
        ));
  }

  Widget list({String? title, GestureTapCallback? onTap}) {
    return ListTile(
      title: Text('$title'),
      trailing: Icon(Icons.arrow_right),
      onTap: onTap,
    );
  }
}
