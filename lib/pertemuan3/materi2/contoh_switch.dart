import 'dart:core';

import 'package:flutter/material.dart';

class ContohSwitch extends StatefulWidget {
  const ContohSwitch({ Key? key }) : super(key: key);

  @override
  _ContohSwitchState createState() => _ContohSwitchState();
}

class _ContohSwitchState extends State<ContohSwitch> {
  bool? _isSwitched = false;
  bool? _isSwitched2 = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Contoh Switch"),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Center(
            child: Switch(
                value: _isSwitched!,
                onChanged: (value) {
                  setState(() {
                    _isSwitched = value;
                  });
                },
                activeColor: Colors.green,
                activeTrackColor: Colors.lightGreenAccent,
              ),
          ),

          Center(
            child: Switch(
              value: _isSwitched2!,
              onChanged: (bool value) {
                setState(() {
                    _isSwitched2 = value;
                  });
              },
              activeColor: Colors.green,
              activeTrackColor: Colors.deepOrange,
              inactiveTrackColor: Colors.cyan,
              inactiveThumbColor: Colors.amberAccent,
            ),
          )
        ],
      ),
    );
  }
}