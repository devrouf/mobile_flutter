import 'package:flutter/material.dart';

class ContohDropdown extends StatefulWidget {
  const ContohDropdown({ Key? key }) : super(key: key);

  @override
  _ContohDropdownState createState() => _ContohDropdownState();
}

class _ContohDropdownState extends State<ContohDropdown> {

  String _dropDownValue = "Pilihan Pertama";
  List<String>? _listDropDownValue = [
    "Pilihan Pertama",
    "Pilihan Kedua",
    "Pilihan Ketiga",
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Contoh DropDown"),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
           Center(
             child: DropdownButton<String>(
                  // icon: const Icon(Icons.arrow_downward),
                  // iconSize: 24,
                  // elevation: 16,
                  // style: const TextStyle(
                  //     color: Colors.deepPurple
                  // ),
                  // underline: Container(
                  //   height: 2,
                  //   color: Colors.deepPurpleAccent,
                  // ),
                  onChanged: (newValue) {
                    setState(() {
                      _dropDownValue = newValue!;
                    });
                  },
                  value: _dropDownValue,
                  items: _listDropDownValue!.map<DropdownMenuItem<String>>((val) {
                    return DropdownMenuItem<String>(value: val, child: Text(val));
                  }).toList()),
           ),
        ],
      ),
    );
  }
}