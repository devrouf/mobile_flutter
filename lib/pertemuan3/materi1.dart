import 'package:flutter/material.dart';
import 'package:mobile_flutter/pertemuan3/materi1/contoh_checkbox.dart';
import 'package:mobile_flutter/pertemuan3/materi1/contoh_form.dart';
import 'package:mobile_flutter/pertemuan3/materi1/contoh_radiobutton.dart';
import 'package:mobile_flutter/pertemuan3/materi1/contoh_textfield.dart';

class P3Materi1 extends StatefulWidget {
  const P3Materi1({Key? key}) : super(key: key);

  @override
  _P3Materi1State createState() => _P3Materi1State();
}

class _P3Materi1State extends State<P3Materi1> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Materi 1"),
      ),
      body: ListView(
        children: [
          ListTile(
            onTap: () {
              Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (route) => FormPage(),
                ),
              );
            },
            title: Text('Form'),
            subtitle: Text('Untuk membuat validasi pada form inpitan'),
            trailing: Icon(Icons.arrow_forward_ios_sharp),
          ),
          ListTile(
            onTap: () {
              Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (route) => TextFormFieldPage(),
                ),
              );
            },
            title: Text('TextFormField'),
            subtitle: Text('Untuk membuat inputan dari pengguna berupa text'),
            trailing: Icon(Icons.arrow_forward_ios_sharp),
          ),
          ListTile(
            onTap: () {
              Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (route) => CheckboxPage(),
                ),
              );
            },
            title: Text('Checkbox'),
            subtitle: Text(
                'Untuk membuat inputan dari pengguna berupa pilihan lebih dari satu'),
            trailing: Icon(Icons.arrow_forward_ios_sharp),
          ),
          ListTile(
            onTap: () {
              Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (route) => RadioPage(),
                ),
              );
            },
            title: Text('Radiobutton'),
            subtitle: Text(
                'Untuk membuat inputan dari pengguna berupa pilihan salah satu'),
            trailing: Icon(Icons.arrow_forward_ios_sharp),
          ),
        ],
      ),
    );
  }
}
