import 'package:flutter/material.dart';
import 'package:mobile_flutter/pertemuan3/materi3/contoh_gridview.dart';
import 'package:mobile_flutter/pertemuan3/materi3/contoh_listview.dart';

class Materi3 extends StatefulWidget {
  const Materi3({Key? key}) : super(key: key);

  @override
  _Materi3State createState() => _Materi3State();
}

class _Materi3State extends State<Materi3> {
  Widget _listItem(int index, {String? title, String? subtitle}) {
    return ListTile(
      onTap: () {
        Navigator.of(context).push(
          MaterialPageRoute(
            builder: (route) => ContohListView(
              index,
              [title, subtitle].where((e) => e != null).join(" - "),
            ),
          ),
        );
      },
      title: Text(title ?? ''),
      subtitle: (subtitle?.isEmpty ?? true) ? null : Text(subtitle!),
      trailing: Icon(Icons.arrow_forward_ios_sharp),
    );
  }

  Widget _gridItem(int index, {String? title, String? subtitle}) {
    return ListTile(
      onTap: () {
        Navigator.of(context).push(
          MaterialPageRoute(
            builder: (route) => ContohGridView(
              index,
              [title, subtitle].where((e) => e != null).join(" - "),
            ),
          ),
        );
      },
      title: Text(title ?? ''),
      subtitle: (subtitle?.isEmpty ?? true) ? null : Text(subtitle!),
      trailing: Icon(Icons.arrow_forward_ios_sharp),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Materi 3")),
      body: ListView(
        children: [
          /*** ListView ***/
          _listItem(
            0,
            title: "Scroll View",
            subtitle: "Column",
          ),
          _listItem(
            1,
            title: "Scroll View",
            subtitle: "Row",
          ),
          _listItem(
            2,
            title: "List View",
          ),
          _listItem(
            3,
            title: "List View",
            subtitle: "Builder",
          ),
          _listItem(
            4,
            title: "List View",
            subtitle: "Builder dengan array",
          ),
          _listItem(
            5,
            title: "List View",
            subtitle: "Builder separated",
          ),

          /*** GridView ***/
          _gridItem(
            0,
            title: "Grid View",
            subtitle: "Count",
          ),
          _gridItem(
            1,
            title: "Grid View",
            subtitle: "Count with fix height",
          ),
          _gridItem(
            2,
            title: "Grid View",
            subtitle: "Count Horizontal",
          ),
          _gridItem(
            3,
            title: "Grid View",
            subtitle: "Builder Horizontal",
          ),
          _gridItem(
            4,
            title: "Grid View",
            subtitle: "Builder horizontal array",
          ),
          _gridItem(
            5,
            title: "Grid View",
            subtitle: "Scroll inside scroll",
          ),
        ],
      ),
    );
  }
}
