import 'package:flutter/material.dart';
import 'package:mobile_flutter/pertemuan9/P9Materi2.dart';

class Pertemuan9Page extends StatefulWidget {
  const Pertemuan9Page({Key? key}) : super(key: key);

  @override
  _Pertemuan9PageState createState() => _Pertemuan9PageState();
}

class _Pertemuan9PageState extends State<Pertemuan9Page> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("SFA Pertemuan 9"),
      ),
      body: P9Materi2(),
    );
  }
}
