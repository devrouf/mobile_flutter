import 'package:flutter/material.dart';
import 'package:mobile_flutter/pertemuan10/materi_notification/contoh_notification.dart';
import 'package:mobile_flutter/pertemuan10/firebase_database_page.dart';

class Pertemuan10Page extends StatefulWidget {
  const Pertemuan10Page({Key? key}) : super(key: key);

  @override
  _Pertemuan10PageState createState() => _Pertemuan10PageState();
}

class _Pertemuan10PageState extends State<Pertemuan10Page> {
  @override
  Widget build(BuildContext context) {
    double size = 60;
    return Scaffold(
      appBar: AppBar(title: Text("SFA Pertemuan 10 - Firebase")),
      body: ListView(
        children: [
          ListTile(
            onTap: () {
              Navigator.of(context).push(
                MaterialPageRoute(builder: (route) => FirebaseDatabasePage()),
              );
            },
            title: Text('Firebase Database Example'),
            leading: Container(
              width: size,
              height: size,
              child: Center(child: Text('Rouf')),
            ),
            trailing: Icon(Icons.arrow_forward_ios_sharp),
          ),
          ListTile(
            onTap: () {
              Navigator.of(context).push(
                MaterialPageRoute(builder: (route) => ContohNotification()),
              );
            },
            title: Text('Firebase Notification'),
            leading: Container(
              width: size,
              height: size,
              child: Center(child: Text('Elfas')),
            ),
            trailing: Icon(Icons.arrow_forward_ios_sharp),
          ),
          ListTile(
            onTap: () {
              // Navigator.of(context).push(MaterialPageRoute(
              //   builder: (route) => P7Materi3(),
              // ));
            },
            title: Text('Maps / Location'),
            leading: Container(
              width: size,
              height: size,
              child: Center(child: Text('Umam')),
            ),
            trailing: Icon(Icons.arrow_forward_ios_sharp),
          ),
        ],
      ),
    );
  }
}
