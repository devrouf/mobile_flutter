import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ColumnPage extends StatefulWidget {
  const ColumnPage({Key? key}) : super(key: key);

  @override
  _ColumnPageState createState() => _ColumnPageState();
}

class _ColumnPageState extends State<ColumnPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Column"),
      ),
      body: Column(
        children: [
          Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(15)),
              color: Colors.red,
            ),
            margin: EdgeInsets.all(8),
            height: 70,
            width: 70,
          ),
          Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(15)),
              color: Colors.green,
            ),
            margin: EdgeInsets.all(8),
            height: 70,
            width: 70,
          ),
          Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(15)),
              color: Colors.blue,
            ),
            margin: EdgeInsets.all(8),
            height: 70,
            width: 70,
          ),
        ],
      ),
    );
  }
}
