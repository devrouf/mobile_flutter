import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class FlexiblePage extends StatefulWidget {
  const FlexiblePage({Key? key}) : super(key: key);

  @override
  _FlexiblePageState createState() => _FlexiblePageState();
}

class _FlexiblePageState extends State<FlexiblePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Flexible"),
      ),
      body: Container(
        child: Column(
          children: [
            Row(
              children: [
                Flexible(
                  child: Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(8)),
                      color: Colors.red,
                    ),
                    margin: EdgeInsets.all(8),
                    padding: EdgeInsets.all(5),
                    height: 70,
                    // child: Text("default", style: TextStyle(fontSize: 30, color: Colors.white)),
                  ),
                ),
                Flexible(
                  flex: 2,
                  child: Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(8)),
                      color: Colors.green,
                    ),
                    margin: EdgeInsets.all(8),
                    padding: EdgeInsets.all(5),
                    height: 70,
                    // child: Text("flex = 1", style: TextStyle(fontSize: 30, color: Colors.white)),
                  ),
                ),
                Flexible(
                  flex: 3,
                  child: Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(8)),
                      color: Colors.blue,
                    ),
                    margin: EdgeInsets.all(8),
                    padding: EdgeInsets.all(5),
                    height: 70,
                    // child: Text("flex = 2", style: TextStyle(fontSize: 30, color: Colors.white)),
                  ),
                ),
              ],
            ),
            Flexible(
              fit: FlexFit.tight,
              child: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(8)),
                  color: Colors.green,
                ),
                margin: EdgeInsets.all(8),
                padding: EdgeInsets.all(5),
                height: 50,
//                child: Text("FlexFit.tight",
//                    style: TextStyle(fontSize: 30, color: Colors.white)),
              ),
            ),
            Flexible(
              fit: FlexFit.loose,
              child: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(8)),
                  color: Colors.green,
                ),
                margin: EdgeInsets.all(8),
                padding: EdgeInsets.all(5),
                height: 310,
//                child: Text("FlexFit.loose",
//                    style: TextStyle(fontSize: 30, color: Colors.white)),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
