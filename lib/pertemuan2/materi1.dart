import 'package:flutter/material.dart';
import 'package:mobile_flutter/pertemuan2/materi1/card.dart';
import 'package:mobile_flutter/pertemuan2/materi1/container.dart';
import 'package:mobile_flutter/pertemuan2/materi1/icon.dart';
import 'package:mobile_flutter/pertemuan2/materi1/image.dart';
import 'package:mobile_flutter/pertemuan2/materi1/text.dart';

class Materi1P2 extends StatefulWidget {
  const Materi1P2({Key? key}) : super(key: key);

  @override
  _Materi1P2State createState() => _Materi1P2State();
}

class _Materi1P2State extends State<Materi1P2> {
  Widget _listTileItem(
      {required Function() onTap, String? title, String subtitle = ''}) {
    return ListTile(
      onTap: onTap,
      title: Text(title ?? ''),
      subtitle: Text(subtitle),
      trailing: Icon(Icons.arrow_forward_ios_sharp),
    );
  }

  @override
  Widget build(BuildContext context) {
    // return IconDemo();
    return Scaffold(
      appBar: AppBar(
        title: Text('Materi1 P2'),
      ),
      body: ListView(
        children: [
          _listTileItem(
            onTap: () {
              Navigator.of(context).push(
                MaterialPageRoute(builder: (route) => ContainerDemo()),
              );
            },
            title: 'Container',
          ),
          _listTileItem(
            onTap: () {
              Navigator.of(context).push(
                MaterialPageRoute(builder: (route) => CardsDemo()),
              );
            },
            title: 'Card',
          ),
          _listTileItem(
            onTap: () {
              Navigator.of(context).push(
                MaterialPageRoute(builder: (route) => TypographyDemo()),
              );
            },
            title: 'Text',
          ),
          _listTileItem(
            onTap: () {
              Navigator.of(context).push(
                MaterialPageRoute(builder: (route) => ImageDemo()),
              );
            },
            title: 'Image',
          ),
          _listTileItem(
            onTap: () {
              Navigator.of(context).push(
                MaterialPageRoute(builder: (route) => IconDemo()),
              );
            },
            title: 'Icon',
          ),
        ],
      ),
    );
  }
}
