import 'dart:async';
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:mobile_flutter/utils/measured_size.dart';

class ImageDemo extends StatefulWidget {
  const ImageDemo({Key? key}) : super(key: key);

  @override
  _ImageDemoState createState() => _ImageDemoState();
}

class _ImageDemoState extends State<ImageDemo> {
  @override
  Widget build(BuildContext context) {
    Future<Size> _calculateImageDimension(AssetImage image) {
      Completer<Size> completer = Completer();
      image.resolve(ImageConfiguration()).addListener(
        ImageStreamListener(
          (ImageInfo image, bool synchronousCall) {
            var myImage = image.image;
            Size size =
                Size(myImage.width.toDouble(), myImage.height.toDouble());
            completer.complete(size);
          },
        ),
      );
      return completer.future;
    }

    var image = AssetImage('assets/images/img_catalina.png');
    _calculateImageDimension(image)
        .then((size) => print("size = $size = ${size.aspectRatio}"));

    var size = MediaQuery.of(context).size;
    var logoSize = Size(407, 156);

    double _sigmaX = 0.0; // from 0-10
    double _sigmaY = 0.0; // from 0-10
    double _opacity = 0.1; // from 0-1.0
    double _width = 350;
    double _height = 300;
    double _blurWidth = _width / 2;
    double _blurHeight = _height / 2;

    return Scaffold(
      appBar: AppBar(title: Text('Image')),
      body: ListView(
        children: [
          Center(
            child: Image(
              image: AssetImage('assets/images/img_catalina.png'),
            ),
          ),
          CircleAvatar(
            backgroundImage: AssetImage('assets/images/img_catalina.png'),
            radius: 84,
          ),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 32),
            child: Image.asset(
              'assets/images/img_logo_forca.png',
            ),
          ),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 32),
            child: Image.asset(
              'assets/images/img_logo_forca.png',
              color: Colors.green,
            ),
          ),
          Container(
            margin: EdgeInsets.symmetric(horizontal: 32),
            child: ClipRRect(
              child: ImageFiltered(
                imageFilter: ImageFilter.blur(sigmaX: 0, sigmaY: 3),
                child: Image.asset(
                  'assets/images/img_logo_forca.png',
                  // color: Colors.green,
                ),
              ),
            ),
          ),
          if (false)
            Column(
              children: [
                Center(
                  child: Stack(
                    // fit: StackFit.expand,
                    children: <Widget>[
                      Image.asset(
                        "assets/images/img_customer_care.PNG",
                        fit: BoxFit.contain,
                      ),
                      Positioned.fill(
                        child: Center(
                          child: BackdropFilter(
                            filter: ImageFilter.blur(
                              sigmaX: 1.0,
                              sigmaY: 1.0,
                            ),
                            child: Container(
                              width: 100,
                              height: 100,
                              color: Colors.black.withOpacity(0.2),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          if (false)
            Center(
              child: Container(
                color: Colors.red,
                height: 300,
                width: 300,
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(16),
                  child: Image(
                    image: image,
                    // fit: BoxFit.contain,
                    // fit: BoxFit.fill,
                  ),
                ),
              ),
            ),
          if (false)
            Center(
              child: Container(
                color: Colors.red,
                height: 300,
                width: 300,
                child: Center(
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(16),
                    child: Image(
                      image: image,
                      // fit: BoxFit.contain,
                    ),
                  ),
                ),
              ),
            ),
          if (false)
            MeasuredSize(
              onChange: (size) {
                print('measure $size = ${size.aspectRatio}');
              },
              child: Image(
                image: image,
                // fit: BoxFit.fill,
                // fit: BoxFit.contain,
                fit: BoxFit.contain,
                // width: 100,
                // fit: BoxFit.fitHeight,
                // fit: BoxFit.fitWidth,
                // fit: BoxFit.cover,
              ),
            ),
          if (false)
            MeasuredSize(
              onChange: (size) {
                print(size);
              },
              child: Column(
                children: [
                  Image(
                    image: AssetImage('assets/images/img_catalina.png'),
                    // width: 100,
                    // fit: BoxFit.fitHeight,
                    // fit: BoxFit.fitWidth,
                    // fit: BoxFit.cover,
                  ),
                ],
              ),
            ),
        ],
      ),
    );
  }
}
