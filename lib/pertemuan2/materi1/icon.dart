import 'package:flutter/material.dart';
import 'package:mobile_flutter/pertemuan2/materi1/my_icons_icons.dart';

class IconDemo extends StatefulWidget {
  const IconDemo({Key? key}) : super(key: key);

  @override
  _IconDemoState createState() => _IconDemoState();
}

class _IconDemoState extends State<IconDemo> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Icon')),
      body: ListView(
        children: [
          Icon(
            Icons.call,
          ),
          Icon(
            Icons.call,
            size: 64,
          ),
          Icon(
            Icons.people,
            size: 64,
            color: Colors.green,
          ),
          Icon(
            Icons.call,
            semanticLabel: 'tes',
            size: 64,
            color: Colors.green,
            textDirection: TextDirection.ltr,
          ),
          Icon(
            Icons.call,
            semanticLabel: 'tes',
            size: 64,
            color: Colors.orange,
            textDirection: TextDirection.rtl,
          ),
          Icon(
            MyIcons.home,
            semanticLabel: 'tes',
            size: 64,
            color: Colors.green,
            textDirection: TextDirection.rtl,
          ),
          Icon(
            MyIcons.home_outline,
            semanticLabel: 'tes',
            size: 64,
            color: Colors.green,
            textDirection: TextDirection.rtl,
          ),
          Icon(
            MyIcons.whatsapp_icon,
            semanticLabel: 'tes',
            size: 64,
            color: Colors.blue,
            textDirection: TextDirection.rtl,
          ),
        ],
      ),
    );
  }
}
