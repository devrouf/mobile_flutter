import 'package:flutter/material.dart';
import 'package:mobile_flutter/pertemuan2/materi1.dart';
import 'package:mobile_flutter/pertemuan2/materi2.dart';
import 'materi3.dart';

class Pertemuan2Page extends StatefulWidget {
  const Pertemuan2Page({Key? key}) : super(key: key);

  @override
  _Pertemuan2PageState createState() => _Pertemuan2PageState();
}

class _Pertemuan2PageState extends State<Pertemuan2Page> {
  @override
  Widget build(BuildContext context) {
    double size = 60;
    return Scaffold(
      appBar: AppBar(
        title: Text('Pertemuan 2'),
      ),
      body: ListView(
        children: [
          ListTile(
            onTap: () {
              Navigator.of(context).push(
                MaterialPageRoute(builder: (route) => Materi1P2()),
              );
            },
            title: Text('Materi 1'),
            subtitle: Text('Container, card, text, image, icon'),
            leading: Container(
              width: size,
              height: size,
              child: Center(child: Text('Rouf')),
            ),
            trailing: Icon(Icons.arrow_forward_ios_sharp),
          ),
          ListTile(
            onTap: () {
              Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (route) => Materi2(),
                ),
              );
            },
            title: Text('Materi 2'),
            subtitle: Text('button (raised, flat, icon, inkwell)'),
            leading: Container(
              width: size,
              height: size,
              child: Center(child: Text('Elfas')),
            ),
            trailing: Icon(Icons.arrow_forward_ios_sharp),
          ),
          ListTile(
            onTap: () {
              Navigator.of(context).push(MaterialPageRoute(
                builder: (route) => Materi3(),
              ));
            },
            title: Text('Materi 3'),
            subtitle: Text(
              'row, column, stack, expanded, flexible',
            ),
            leading: Container(
              width: size,
              height: size,
              child: Center(child: Text('Umam')),
            ),
            trailing: Icon(Icons.arrow_forward_ios_sharp),
          ),
        ],
      ),
    );
  }
}
