import 'package:flutter/material.dart';

class ContohRaisedButton extends StatefulWidget {
  const ContohRaisedButton({Key? key}) : super(key: key);

  @override
  _ContohRaisedButtonState createState() => _ContohRaisedButtonState();
}

class _ContohRaisedButtonState extends State<ContohRaisedButton> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Contoh Raised Button"),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Center(
            child: RaisedButton(
              color: Colors.amberAccent,
              child: Text("Ini RaisedButton"),
              elevation: 10,
              // Cara untuk disable
              // onLongPress: null,
              onPressed: () {
                print("Saya telah klik RaisedButton");
              },
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10)),
            ),
          ),
          Center(
            child: Container(
              child: RaisedButton.icon(
                  onPressed: () {
                    print("Contoh button yanhg ada iconnya");
                  },
                  icon: Icon(Icons.star),
                  label: Text("Contoh Icon Button")),
            ),
          ),
          // untuk maximize ukuran button
          Container(
            width: double.infinity,
            child: RaisedButton(
              child: Text("Button Full Width"),
              onPressed: () {
                print("Ini Contoh Full width");
              },
            ),
          ),
        ],
      ),
    );
  }
}
