import 'package:flutter/material.dart';

class ContohIconButton extends StatefulWidget {
  const ContohIconButton({ Key? key }) : super(key: key);

  @override
  _ContohIconButtonState createState() => _ContohIconButtonState();
}

class _ContohIconButtonState extends State<ContohIconButton> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(""),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Center(
            child: IconButton(
              icon: Icon(Icons.work),
              onPressed: () {
                print("Saya telah klik IconButton");
              },
            ),
          ),
        ],
      ),
    );
  }
}