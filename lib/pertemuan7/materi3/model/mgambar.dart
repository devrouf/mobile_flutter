class Mgambar {
  String? status;
  String? message;
  Gambar? data;

  Mgambar({this.status, this.message, this.data});

  Mgambar.fromJson(dynamic json) {
    status = json["status"];
    message = json["message"];
    data = json["data"] != null ? Gambar.fromJson(json["data"]) : null;
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["status"] = status;
    map["message"] = message;
    if (data != null) {
      map["data"] = data?.toJson();
    }
    return map;
  }
}

class Gambar {
  String? imageName;

  Gambar({this.imageName});

  Gambar.fromJson(dynamic json) {
    imageName = json["image_name"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["image_name"] = imageName;
    return map;
  }
}
