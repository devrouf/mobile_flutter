import 'package:flutter/material.dart';
import 'package:get_storage/get_storage.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ContohPersistence extends StatefulWidget {
  const ContohPersistence({Key? key}) : super(key: key);

  @override
  _ContohPersistenceState createState() => _ContohPersistenceState();
}

class _ContohPersistenceState extends State<ContohPersistence> {
  SharedPreferences? pref;
  String? data1;
  String? data2;
  String? data3;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Data Persistence")),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Text(
              "GetStorage",
              style: Theme.of(context).textTheme.headline6,
            ),
            Text("Data1 = $data1"),
            Row(
              children: [
                ElevatedButton(
                  onPressed: () async {
                    await GetStorage.init();
                    print("init get storage");
                  },
                  child: Text("Init"),
                ),
                ElevatedButton(
                  onPressed: () {
                    GetStorage pref = GetStorage();
                    pref.write("token",
                        "Bearer ${DateTime.now().millisecondsSinceEpoch}");
                  },
                  child: Text("Set"),
                ),
                ElevatedButton(
                  onPressed: () {
                    GetStorage pref = GetStorage();
                    print("token ${pref.read("token")}");
                    setState(() {
                      data1 = pref.read("token");
                    });
                  },
                  child: Text("Get"),
                ),
                ElevatedButton(
                  onPressed: () {
                    GetStorage pref = GetStorage();
                    pref.erase();
                  },
                  child: Text("Clear"),
                ),
              ],
            ),
            SizedBox(height: 24),
            Divider(thickness: 2),
            Text(
              "Shared Preferences 1",
              style: Theme.of(context).textTheme.headline6,
            ),
            Text("Menggunakan init di setiap pemanggilan"),
            Text(""),
            Text("Data2 = $data2"),
            Row(
              children: [
                ElevatedButton(
                  onPressed: () async {
                    print("tidak ada init");
                  },
                  child: Text("Init"),
                ),
                ElevatedButton(
                  onPressed: () async {
                    SharedPreferences pref =
                        await SharedPreferences.getInstance();
                    pref.setString("token",
                        "Bearer ${DateTime.now().millisecondsSinceEpoch}");
                  },
                  child: Text("Set"),
                ),
                ElevatedButton(
                  onPressed: () async {
                    SharedPreferences pref =
                        await SharedPreferences.getInstance();
                    print("token ${pref.getString("token")}");
                    setState(() {
                      data2 = pref.getString("token");
                    });
                  },
                  child: Text("Get"),
                ),
                ElevatedButton(
                  onPressed: () async {
                    SharedPreferences pref =
                        await SharedPreferences.getInstance();
                    pref.clear();
                  },
                  child: Text("Clear"),
                ),
              ],
            ),
            SizedBox(height: 24),
            Divider(thickness: 2),
            Text(
              "Shared Preferences 2",
              style: Theme.of(context).textTheme.headline6,
            ),
            Text("Tanpa init di setiap pemanggilan,"),
            Text("namun wajib init di awal penggunaan"),
            Text(""),
            Text("Status init Pref = $pref"),
            Text("Status token = ${pref?.containsKey("token")}"),
            Text("pref['token'] = ${pref?.getString("token")}"),
            Text("Data3 = $data3"),
            Row(
              children: [
                ElevatedButton(
                  onPressed: () async {
                    pref = await SharedPreferences.getInstance();
                    setState(() {});
                  },
                  child: Text("Init"),
                ),
                ElevatedButton(
                  onPressed: () {
                    pref?.setString("token",
                        "Bearer ${DateTime.now().millisecondsSinceEpoch}");
                    setState(() {});
                  },
                  child: Text("Set"),
                ),
                ElevatedButton(
                  onPressed: () {
                    print("token ${pref?.getString("token")}");
                    setState(() {
                      data3 = pref?.getString("token");
                    });
                  },
                  child: Text("Get"),
                ),
                ElevatedButton(
                  onPressed: () async {
                    SharedPreferences pref =
                        await SharedPreferences.getInstance();
                    pref.clear();
                    setState(() {});
                  },
                  child: Text("Clear"),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
