import 'package:flutter/material.dart';
import 'package:get_storage/get_storage.dart';
import 'package:mobile_flutter/pertemuan7/materi3/home_page.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  bool isLoading = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Login Page")),
      body: Center(
        child: isLoading
            ? CircularProgressIndicator()
            : ElevatedButton(
          onPressed: () {
            setState(() {
              isLoading = true;
            });
            GetStorage storage = GetStorage();
            storage.write("token", "Bearer ${DateTime.now().millisecondsSinceEpoch}");
            print("set token");
            Navigator.pushReplacement(
              context,
              MaterialPageRoute(builder: (_) => HomePage()),
            );
          },
          child: Text("Login"),
        ),
      ),
    );
  }
}
