import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:image_picker/image_picker.dart';
import 'package:mobile_flutter/pertemuan7/materi3/model/login.dart';
import 'package:mobile_flutter/pertemuan7/materi3/model/mgambar.dart';
import 'package:mobile_flutter/pertemuan7/materi3/model/mlogin.dart';

class LoginState extends ChangeNotifier {
  TextEditingController uname = new TextEditingController();
  TextEditingController pass = new TextEditingController();
  final formKey = GlobalKey<FormState>();
  String? warning;
  Login? data;
  bool? muncul = false;

  prosesLogin() {
    this.muncul = false;
    this.warning = "";
    this.data = null;
    notifyListeners();
    Uri url = Uri.parse("https://sfa.forcapos.xyz/api/account/login");
    var header;
    var body = {
      "m_akun_namapengguna": uname.text,
      "m_akun_password": pass.text
    };
    http.post(url, headers: header, body: body).then((value) {
      print(value.body);
      try {
        if (value.body.isNotEmpty) {
          var json = jsonDecode(value.body);
          Mlogin dataStatus = Mlogin.fromJson(json);
          if(dataStatus.status!.toUpperCase() == "OK"){
            if(dataStatus.data!.length > 0){
              data = dataStatus.data!.first;
              this.muncul = true;
            } else {
              this.warning = "Data kosong";
            }
          } else {
            this.warning = dataStatus.message;
          }
        } else {
          this.warning = "Data tidak ada";
        }
      } catch(e) {
        this.warning = "Ups.. Sepertinya ada masalah";
      }
      notifyListeners();
    });
  }

  File? image;
  String? urlImage;

  addFoto() {
    this.image = null;
    notifyListeners();
    ImagePicker()
        .pickImage(source: ImageSource.camera, imageQuality: 20)
        .then((value) {
      image = File(value!.path);
      notifyListeners();
    });
  }

  kirimGambar() async {
    this.warning = "";
    this.urlImage = "";
    notifyListeners();
    String url = "https://sfa.forcapos.xyz/api/uploadImage";
    var request = http.MultipartRequest('POST', Uri.parse(url));
    request.files.add(await http.MultipartFile.fromPath(
        'image', image!.path));
    request.send().then((value){
      value.stream.transform(utf8.decoder).join().then((res){
        Map<String, dynamic> json = jsonDecode(res);
        print("RESPON BODY = $res - ${json["status"]}");
        if (value.statusCode == 200 && json["status"].toUpperCase() == "OK") {
          this.urlImage = Mgambar.fromJson(json).data!.imageName;
        } else {
          this.warning = "Ups.. Sepertinya ada masalah";
        }
        notifyListeners();
      });
    });
  }
}
