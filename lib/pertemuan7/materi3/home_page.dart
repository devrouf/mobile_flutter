import 'package:flutter/material.dart';
import 'package:get_storage/get_storage.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    GetStorage storage = GetStorage();
    return Scaffold(
      appBar: AppBar(title: Text("Home Page")),
      body: Center(
        child: Column(
          children: [
            Text("Token: ${storage.read("token")}"),
            ElevatedButton(
              onPressed: () async {
                await GetStorage.init();
                GetStorage storage = GetStorage();
                storage.erase();
                Navigator.popUntil(context, (route) => route.isFirst);
              },
              child: Text("Logout"),
            ),
          ],
        ),
      ),
    );
  }
}
