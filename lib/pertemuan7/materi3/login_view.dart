import 'package:flutter/material.dart';
import 'package:mobile_flutter/pertemuan7/materi3/login_state.dart';
import 'package:provider/provider.dart';

class LoginView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (_) => LoginState(),
      child: Consumer<LoginState>(builder: (context, state, _) {
        return Scaffold(
          appBar: AppBar(
            title: Text("Login"),
          ),
          body: Center(
            child: ListView(
              children: [
                Form(
                  key: state.formKey,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: TextFormField(
                          controller: state.uname,
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return 'Field ini wajib diisi';
                            }
                            return null;
                          },
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: TextFormField(
                          controller: state.pass,
                          obscureText: true,
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return 'Field ini wajib diisi';
                            }
                            return null;
                          },
                        ),
                      ),
                      if (state.warning != null) Text("${state.warning}"),
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 16.0),
                        child: ElevatedButton(
                          onPressed: () {
                            if (state.formKey.currentState!.validate()) {
                              state.prosesLogin();
                            }
                          },
                          child: const Text('Masuk'),
                        ),
                      ),
                      if (state.muncul == true && state.data != null)
                        Text("Hasil : ${state.data!.mAkunEmail}")
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 16.0),
                  child: ElevatedButton(
                    onPressed: () {
                      state.addFoto();
                    },
                    child: const Text('Ambil Gambar'),
                  ),
                ),
                if (state.image != null)
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 16.0),
                    child: ElevatedButton(
                      onPressed: () {
                        state.kirimGambar();
                      },
                      child: const Text('Kirim'),
                    ),
                  ),
                if (state.urlImage != null && state.urlImage != "")
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 16.0),
                    child: Image.network(
                      "https://sfa.forcapos.xyz/images/${state.urlImage}",
                      width: MediaQuery.of(context).size.width - 52,
                      height: MediaQuery.of(context).size.width - 52,
                      fit: BoxFit.cover,
                    ),
                  ),
              ],
            ),
          ),
        );
      }),
    );
  }
}
