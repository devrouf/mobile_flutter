import 'dart:convert';

import 'package:flutter/material.dart';

class ContohArrayJSON extends StatefulWidget {
  const ContohArrayJSON({Key? key}) : super(key: key);

  @override
  _ContohArrayJSONState createState() => _ContohArrayJSONState();
}

class _ContohArrayJSONState extends State<ContohArrayJSON> {
  var _arrayJSON = '{"materi":[{"nama_materi":"dart","jumlah_jam":18},{"nama_materi":"flutter","jumlah_jam":25},{"nama_materi":"PM","jumlah_jam":20}]}';
  var _parseJSON;
  List<dynamic>? _listArray;

  @override
  void initState() {
    super.initState();
    setState(() {
      _parseJSON = json.decode(_arrayJSON);
      _listArray = List.from(_parseJSON['materi']);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Array of JSON'),
      ),
      body: Container(
        child: ListView.builder(
          itemCount: _listArray!.length,
            itemBuilder: (ctx, index) {
              var parseJsonTags = _listArray![index];
              return ListTile(
                title: Text('Materi ${parseJsonTags['nama_materi']}'),
                subtitle: Text('Waktu ${parseJsonTags['jumlah_jam']} jam'),
              );
            }),
      ),
    );
  }
}
