import 'dart:convert';

import 'package:flutter/material.dart';

class ContohMap extends StatefulWidget {
  const ContohMap({Key? key}) : super(key: key);

  @override
  _ContohMapState createState() => _ContohMapState();
}

class _ContohMapState extends State<ContohMap> {
  String? _hasilConvert = "";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Contoh nge-Map'),
      ),
      body: Container(
        alignment: Alignment.center,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text('Contoh JSON'),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text('Hasil '),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text('$_hasilConvert'),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: ElevatedButton(
                child: Text('Convert to JSON'),
                onPressed: () {
                  setState(() {
                    _hasilConvert = _maptoJSON();
                  });
                },
              ),
            ),
          ],
        ),
      ),
    );
  }

  String _maptoJSON() {
    Map<String, dynamic> datatoJSON = {};

    datatoJSON["nama"] = "Zayed Elfasa";
    datatoJSON["pekerjaan"] = "Flutter dev";
    datatoJSON["lokasi_kerja"] = "tubanan";

    // var convert = json.encode(datatoJSON);

    var converttoJSON = JsonEncoder().convert(datatoJSON);
    return converttoJSON;
  }
}
