import 'data.dart';

class Jadwal {
  String? status;
  String? title;
  List<Data>? data;

  Jadwal({
      this.status, 
      this.title, 
      this.data});

  Jadwal.fromJson(dynamic json) {
    status = json['status'];
    title = json['title'];
    if (json['data'] != null) {
      data = [];
      json['data'].forEach((v) {
        data?.add(Data.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map['status'] = status;
    map['title'] = title;
    if (data != null) {
      map['data'] = data?.map((v) => v.toJson()).toList();
    }
    return map;
  }

}