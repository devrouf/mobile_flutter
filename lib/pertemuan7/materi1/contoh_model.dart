import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:mobile_flutter/pertemuan7/materi1/model/jadwal/data.dart';
import 'package:mobile_flutter/pertemuan7/materi1/model/jadwal/jadwal.dart';

class ContohModel extends StatefulWidget {
  const ContohModel({Key? key}) : super(key: key);

  @override
  _ContohModelState createState() => _ContohModelState();
}

class _ContohModelState extends State<ContohModel> {

  String dataJSON = '{"status":"Success","title":"Jadwal Flutter Academy","data":[{"id":"001","nama_pemateri":"elfas","taks":["JSON","Model","Map"]},{"id":"002","nama_pemateri":"Umam","taks":["http","Dio","interceptor","upload"]},{"id":"003","nama_pemateri":"Rouf","taks":["Persistence","async","Future"]}]}';
  Jadwal? modelJadwal;

  @override
  void initState() {
    super.initState();
    _decodeJadwal();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Contoh Model"),
      ),
      body: Container(
        child: ListView.builder(
            itemCount: modelJadwal!.data!.length,
            itemBuilder: (BuildContext ctx, int index) {
              Data dataJadwal = modelJadwal!.data![index];
              // title untuk mengisi judul materi
              // subtitle untuk mengisi task
              return ListTile(
                title: Text('${dataJadwal.namaPemateri!}'),
                subtitle: Row(
                  children: listTaks(dataJadwal),
                ),
              );
            }),
      ),
    );
  }

  void _decodeJadwal() {
    setState(() {
      modelJadwal = Jadwal.fromJson(json.decode(dataJSON));
    });
  }

  List<Widget> listTaks(Data? dataJadwal) {
    List<Widget>? list = [];
    for (String task in dataJadwal!.taks!) {
      list.add(Text('$task, '));
    }
    return list;
  }
}

