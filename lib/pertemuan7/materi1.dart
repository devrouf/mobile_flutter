import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:mobile_flutter/pertemuan7/materi1/contoh_arrayjson.dart';
import 'package:mobile_flutter/pertemuan7/materi1/contoh_json.dart';
import 'package:mobile_flutter/pertemuan7/materi1/contoh_map.dart';
import 'package:mobile_flutter/pertemuan7/materi1/contoh_model.dart';
import 'package:mobile_flutter/pertemuan7/materi_dio/contoh_interceptor.dart';
import 'package:mobile_flutter/pertemuan7/materi_dio/contoh_upload.dart';

class P7Materi1 extends StatefulWidget {
  const P7Materi1({Key? key}) : super(key: key);

  @override
  _P7Materi1State createState() => _P7Materi1State();
}

class _P7Materi1State extends State<P7Materi1> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Contoh JSON'),
      ),
      body: Column(
        children: [
          list(
              title: 'Contoh JSON',
              onTap: () {
                Navigator.push(context, MaterialPageRoute(builder: (BuildContext ctx) => ContohJson()));
              }
          ),
          list(
              title: 'Contoh Array JSON',
              onTap: () {
                Navigator.push(context, MaterialPageRoute(builder: (BuildContext ctx) => ContohArrayJSON()));
              }
          ),
          list(
              title: 'Contoh Map to JSON',
              onTap: () {
                Navigator.push(context, MaterialPageRoute(builder: (BuildContext ctx) => ContohMap()));
              }
          ),
          list(
              title: 'Contoh Object Model',
              onTap: () {
                Navigator.push(context, MaterialPageRoute(builder: (BuildContext ctx) => ContohModel()));
              }
          ),
          list(
              title: 'Contoh Interceptor (Dio)',
              onTap: () {
                Navigator.push(context, MaterialPageRoute(builder: (BuildContext ctx) => ContohDioInterceptor()));
              }
          ),
          list(
              title: 'Contoh Upload (Dio)',
              onTap: () {
                Navigator.push(context, MaterialPageRoute(builder: (BuildContext ctx) => ContohDio()));
              }
          ),
        ],
      ),
    );
  }

  Widget list({String? title, GestureTapCallback? onTap}) {
    return ListTile(
      title: Text('$title'),
      trailing: Icon(Icons.arrow_right),
      onTap: onTap,
    );
  }
}




