import 'package:flutter/material.dart';
import 'package:mobile_flutter/pertemuan4/materi1.dart';
import 'package:mobile_flutter/pertemuan4/materi2.dart';
import 'package:mobile_flutter/pertemuan4/materi3.dart';

class Pertemuan4Page extends StatefulWidget {
  const Pertemuan4Page({Key? key}) : super(key: key);

  @override
  _Pertemuan4PageState createState() => _Pertemuan4PageState();
}

class _Pertemuan4PageState extends State<Pertemuan4Page> {
  double size = 60;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Pertemuan 4'),),
      body: ListView(
        children: [
          ListTile(
            onTap: () {
              Navigator.of(context).push(
                MaterialPageRoute(builder: (route) => P4Materi1()),
              );
            },
            title: Text('Materi 1'),
            subtitle: Text('app bar, title, leading, action, willpopscope'),
            leading: Container(
              width: size,
              height: size,
              child: Center(child: Text('Elfas')),
            ),
            trailing: Icon(Icons.arrow_forward_ios_sharp),
          ),
          ListTile(
            onTap: () {
             Navigator.of(context).push(
               MaterialPageRoute(
                 builder: (route) => P4Materi2(),
               ),
             );
            },
            title: Text('Materi 2'),
            subtitle: Text('drawer, popup menu button'),
            leading: Container(
              width: size,
              height: size,
              child: Center(child: Text('Rouf')),
            ),
            trailing: Icon(Icons.arrow_forward_ios_sharp),
          ),
          ListTile(
            onTap: () {
              Navigator.of(context).push(MaterialPageRoute(
                builder: (route) => P4Materi3(),
              ));
            },
            title: Text('Materi 3'),
            subtitle: Text(
              'bottom navigation, tab view, floating button',
            ),
            leading: Container(
              width: size,
              height: size,
              child: Center(child: Text('Umam')),
            ),
            trailing: Icon(Icons.arrow_forward_ios_sharp),
          ),
        ],
      ),
    );
  }
}
