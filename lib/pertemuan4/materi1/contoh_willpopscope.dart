import 'package:flutter/material.dart';

class ContohWillPopScope extends StatefulWidget {
  const ContohWillPopScope({Key? key}) : super(key: key);

  @override
  _ContohWillPopScopeState createState() => _ContohWillPopScopeState();
}

class _ContohWillPopScopeState extends State<ContohWillPopScope> {
  bool? _isBack = true;

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        await showDialog(
            context: context,
            builder: (BuildContext context) {
              return AlertDialog(
                title: Text("Keluar Aplikasi"),
                content: Text("Keluar Aplikasi?"),
                actions: [
                  ElevatedButton(
                      onPressed: () {
                        setState(() {
                          _isBack = true;
                        });
                        Navigator.of(context).pop();
                      },
                      child: Text("Ya")),
                  ElevatedButton(
                      onPressed: () {
                        setState(() {
                          _isBack = false;
                        });
                        Navigator.of(context).pop();
                      },
                      child: Text("Tidak")),
                ],
              );
            });

        return _isBack!;
      },
      child: Scaffold(
        appBar: AppBar(
          title: Text("Contoh WillPopScope"),
        ),
        body: Container(
          alignment: Alignment.center,
          child: Text("Contoh Penggunaan WillPopScope")
        ),
      ),
    );
  }
}
