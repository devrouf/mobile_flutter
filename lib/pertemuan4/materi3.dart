import 'package:flutter/material.dart';
import 'package:mobile_flutter/pertemuan3/materi1/contoh_checkbox.dart';
import 'package:mobile_flutter/pertemuan3/materi1/contoh_radiobutton.dart';
import 'package:mobile_flutter/pertemuan3/materi1/contoh_textfield.dart';
import 'package:mobile_flutter/pertemuan4/materi3/bottom_nav_bar.dart';
import 'package:mobile_flutter/pertemuan4/materi3/floating_button.dart';
import 'package:mobile_flutter/pertemuan4/materi3/tab_view.dart';

class P4Materi3 extends StatefulWidget {
  const P4Materi3({Key? key}) : super(key: key);

  @override
  _P4Materi3State createState() => _P4Materi3State();
}

class _P4Materi3State extends State<P4Materi3> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Materi 3"),
      ),
      body: ListView(
        children: [
          ListTile(
            onTap: () {
              Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (route) => BottomNavBarPage(),
                ),
              );
            },
            title: Text('Bottom Nav Bar'),
            subtitle: Text('Untuk membuat Navigasi Bar dibawah'),
            trailing: Icon(Icons.arrow_forward_ios_sharp),
          ),
          ListTile(
            onTap: () {
              Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (route) => TabBarViewPage(),
                ),
              );
            },
            title: Text('Tab Bar'),
            subtitle: Text('Untuk membuat tampilan berbentuk tab'),
            trailing: Icon(Icons.arrow_forward_ios_sharp),
          ),
          ListTile(
            onTap: () {
              Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (route) => FloatingActionButtonPage(),
                ),
              );
            },
            title: Text('Floating Action Button'),
            subtitle: Text(
                'Untuk membuat tombol dengan efek mengapung'),
            trailing: Icon(Icons.arrow_forward_ios_sharp),
          ),
        ],
      ),
    );
  }
}
