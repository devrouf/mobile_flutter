/*
 * Create by roufroufrouf on 2021
 * Email: roufroufrouf@gmail.com
 * Last modified 9/2/21, 9:15 PM
 */

import 'package:flutter/material.dart';

class ContohDoubleDrawer extends StatefulWidget {
  const ContohDoubleDrawer({Key? key}) : super(key: key);

  @override
  _ContohDoubleDrawerState createState() => _ContohDoubleDrawerState();
}

class _ContohDoubleDrawerState extends State<ContohDoubleDrawer> {
  Widget _contohDrawer() {
    return Drawer(
      child: ListView(
        children: [
          DrawerHeader(
            child: Container(
              color: Colors.green,
            ),
          ),
          ListTile(
            leading: Icon(Icons.people),
            title: Text("Daftar Pelanggan"),
          ),
          ListTile(
            leading: Icon(Icons.inbox),
            title: Text("Inbox"),
          ),
          ...List.generate(
            15,
            (index) => ListTile(
              leading: Icon(Icons.inbox),
              title: Text("Inbox"),
              onTap: () {
                Navigator.pop(context);
              },
            ),
          ),
        ],
      ),
    );
  }

  Widget _contohEndDrawer() {
    return Drawer(
      child: Column(
        children: [
          AppBar(
            title: Text("Flash Sale"),
            centerTitle: true,
            automaticallyImplyLeading: false,
            actions: [
              Icon(
                Icons.lightbulb,
                color: Colors.transparent,
              ),
            ],
          ),
          Expanded(
            child: GridView.count(
              crossAxisCount: 1,
              padding: EdgeInsets.zero,
              childAspectRatio: 16 / 9,
              children:
                  List.generate(10, (index) => Card(child: FlutterLogo())),
            ),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Contoh Double Drawer")),
      body: Center(
        child: ElevatedButton.icon(
          icon: Icon(Icons.arrow_back_ios_rounded),
          label: Text("Kembali"),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
      ),
      drawer: _contohDrawer(),
      endDrawer: _contohEndDrawer(),
      onDrawerChanged: (isOpen) {
        print("status drawer $isOpen");
      },
    );
  }
}
