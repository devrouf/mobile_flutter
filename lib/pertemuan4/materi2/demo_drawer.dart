import 'dart:math';

import 'package:flutter/material.dart';
import 'package:mobile_flutter/pertemuan3/materi3/contoh_gridview.dart';
import 'package:mobile_flutter/pertemuan3/materi3/contoh_listview.dart';

class Data {
  String? produk;
  int? qty;
  int nilai = 0;

  Data({this.produk, this.qty});
}

class DemoDrawer extends StatefulWidget {
  const DemoDrawer({Key? key}) : super(key: key);

  @override
  _DemoDrawerState createState() => _DemoDrawerState();
}

class _DemoDrawerState extends State<DemoDrawer> {
  List<Data> listData = [];

  int indexMenu = 0;

  Widget _contohDrawer() {
    return Drawer(
      child: ListView(
        children: [
          DrawerHeader(
            child: Container(
              color: Colors.green,
            ),
          ),
          ListTile(
            leading: Icon(Icons.home),
            title: Text("Beranda"),
            onTap: () {
              indexMenu = 0;
              Navigator.pop(context);
              setState(() {});
            },
          ),
          ListTile(
            leading: Icon(Icons.people),
            title: Text("Contoh listView"),
            onTap: () {
              indexMenu = 1;
              Navigator.pop(context);
              setState(() {});
            },
          ),
          ListTile(
            leading: Icon(Icons.inbox),
            title: Text("Contoh GridView"),
            onTap: () {
              indexMenu = 2;
              Navigator.pop(context);
              setState(() {});
            },
          ),
        ],
      ),
    );
  }

  Widget _layoutBeranda() {
    if (listData.isEmpty) {
      return Center(
        child: Text(
          "Daftar Kosong\nSilakan Tambah (+) Data",
          textAlign: TextAlign.center,
        ),
      );
    }

    return ListView.builder(
      itemCount: listData.length,
      itemBuilder: (bc, idx) {
        final data = listData[idx];
        return ListTile(
          title: Text(data.produk ?? ""),
          subtitle: Text("Qty: ${data.qty}"),
          // trailing: Icon(Icons.more_horiz),
          trailing: PopupMenuButton<int>(
            itemBuilder: (buildContext) {
              return [
                PopupMenuItem(child: Text("Hapus"), value: 0),
                PopupMenuItem(child: Text("Ubah"), value: 1),
              ];
            },
            onSelected: (value) {
              if (value == 0) {
                print("hapus $value");
                listData.removeAt(idx);
              } else if (value == 1) {
                print("ubah $value");
                data.qty = Random().nextInt(20) + 20;
              }
              setState(() {});
            },
          ),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          if (indexMenu == 0) Expanded(child: _layoutBeranda()),
          if (indexMenu == 1) Expanded(child: ContohListView(0, "ListView")),
          if (indexMenu == 2) Expanded(child: ContohGridView(0, "GridView")),
        ],
      ),
      drawer: _contohDrawer(),
      endDrawer: _contohDrawer(),
      appBar: AppBar(
        centerTitle: true,
        title: Column(
          children: [
            Text("Live Demo"),
            Text(
              "(Drawer+PopupMenuButton)",
              style: Theme.of(context).textTheme.caption?.copyWith(
                    color: Colors.white,
                  ),
            ),
          ],
        ),
        actions: [
          IconButton(
            onPressed: () {
              int qty = Random().nextInt(20);
              listData.add(Data(
                produk: "Semen $qty",
                qty: qty,
              ));
              setState(() {});
            },
            icon: Icon(Icons.add),
          ),
        ],
      ),
    );
  }
}
