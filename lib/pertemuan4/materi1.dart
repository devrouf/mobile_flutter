import 'package:flutter/material.dart';
import 'package:mobile_flutter/pertemuan4/materi1/contoh_appbar.dart';
import 'package:mobile_flutter/pertemuan4/materi1/contoh_willpopscope.dart';

class P4Materi1 extends StatefulWidget {
  const P4Materi1({Key? key}) : super(key: key);

  @override
  _P4Materi1State createState() => _P4Materi1State();
}

class _P4Materi1State extends State<P4Materi1> {
  List<String> _listMenuPopUp = ["Pengaturan", "Keluar"];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text("Contoh AppBar"),
          leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () {
              print("On Pressed");
            },
          ),
        ),
        body: ListView(
          children: [
            list(
                title: "Contoh AppBar",
                onTap: () {
                  Navigator.of(context).push(
                    MaterialPageRoute(builder: (route) => ContohAppBar()),
                  );
                }),
            list(
                title: "Contoh WillPopScope",
                onTap: () {
                  Navigator.of(context).push(
                    MaterialPageRoute(builder: (route) => ContohWillPopScope()),
                  );
                })
          ],
        )
    );
  }

  Widget list({String? title, GestureTapCallback? onTap}) {
    return ListTile(
      title: Text('$title'),
      trailing: Icon(Icons.arrow_right),
      onTap: onTap,
    );
  }
}
